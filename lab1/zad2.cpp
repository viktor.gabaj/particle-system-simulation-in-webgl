#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <GL/glut.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

GLuint window;
GLuint width = 600, height = 600;
int counter = 0;

void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void idle();

struct Matrica3x3 {
    double ax;
    double bx;
    double cx;
    double ay; 
    double by;
    double cy;
    double az;
    double bz;
    double cz;
} Matrica3x3;

struct Vektor {
    double i;
    double j;
    double k;
} Vektor;

struct dot {
	double x;
	double y;
	double z;
	double h;
} dot;

struct poli {
	int p1, p2, p3;
	double A, B, C, D;
} poli;

double calculatePosition(double r0, double r1, double r2, double r3, double t) {
	return (double)1/6 * ((-1* pow(t, 3) + 3*pow(t, 2) - 3*t + 1)*r0 + (3*pow(t, 3) - 6*pow(t, 2) + 4)*r1 + (-3*pow(t, 3) + 3*pow(t, 2) + 3*t + 1)*r2 + pow(t, 3)*r3);
}

double calculateDerivation(double r0, double r1, double r2, double r3, double t) {
	return (double)1/2 * ((-1*pow(t, 2) + 2*t - 1)*r0 + (3*pow(t,2) - 4*t)*r1 + (-3*pow(t, 2) + 2*t + 1)*r2 + pow(t, 2)*r3);
}

double calculate2Derivation(double r0, double r1, double r2, double r3, double t) {
	return (1-t)*r0 + (3*t-2)*r1 + (1-3*t)*r2 + t*r3;
}

struct Vektor vektorskiUmnozak(struct Vektor v1, struct Vektor v2) {
    struct Vektor rez;
    rez.i = v1.j*v2.k - v2.j*v1.k;
    rez.j = -v1.i*v2.k + v2.i*v1.k;
    rez.k = v1.i*v2.j - v2.i*v1.j;
    return rez;
};

void ispisiVektor(struct Vektor v){
    printf("[ %.2fi %.2fj %.2fk ]\n", v.i, v.j, v.k);
};

struct Vektor normirajVektor(struct Vektor v){
    struct Vektor rez;
    double naz = sqrt(v.i*v.i + v.j*v.j + v.k*v.k);  
    rez.i = v.i / naz;
    rez.j = v.j / naz;
    rez.k = v.k / naz;
    return rez;
};

struct Matrica3x3 invertirajMatricu(struct Matrica3x3 m) {
    struct Matrica3x3 rez;
    struct Matrica3x3 c;
    double det = m.ax*(m.by*m.cz - m.cy*m.bz) - m.bx*(m.ay*m.cz - m.cy*m.az) + m.cx*(m.ay*m.bz - m.by*m.az);
    
    c.ax = m.by*m.cz - m.cy*m.bz;
    c.bx = m.cx*m.bz - m.bx*m.cz;
    c.cx = m.bx*m.cy - m.cx*m.by;
    c.ay = m.cy*m.az - m.ay*m.cz;
    c.by = m.ax*m.cz - m.cx*m.az;
    c.cy = m.cx*m.ay - m.cy*m.ax;
    c.az = m.ay*m.bz - m.by*m.az;
    c.bz = m.bx*m.az - m.ax*m.bz;
    c.cz = m.ax*m.by - m.bx*m.ay;
    
    rez.ax = c.ax / det;
    rez.bx = c.bx / det;
    rez.cx = c.cx / det;
    rez.ay = c.ay / det;
    rez.by = c.by / det;
    rez.cy = c.cy / det;
    rez.az = c.az / det;
    rez.bz = c.bz / det;
    rez.cz = c.cz / det;
    
    return rez;
}

void ispisiMatricu(struct Matrica3x3 m){
    printf("| %6.2f %6.2f %6.2f |\n", m.ax, m.bx, m.cx);
    printf("| %6.2f %6.2f %6.2f |\n", m.ay, m.by, m.cy);
    printf("| %6.2f %6.2f %6.2f |\n", m.az, m.bz, m.cz);
}


struct dot *vrhovi;
struct poli *poligoni;
int vc = 0, pc = 0;

int crtajTangente = 1;

double xmax = 0, ymax = 0;

struct dot ociste, glediste;

struct dot tockeKrivulje[12];
struct dot tockePutanje[900];
struct dot tangentePutanje[900];
struct dot tangente2Putanje[900];

int main(int argc, char ** argv){

    // Tocke krivulje koju crtamo
	tockeKrivulje[0] = {0, 0, 0};
	tockeKrivulje[1] = {0, 5, 2.5};
	tockeKrivulje[2] = {5, 5, 5};
	tockeKrivulje[3] = {5, 0, 7.5};
	tockeKrivulje[4] = {0, 0, 10};
	tockeKrivulje[5] = {0, 5, 12.5};
	tockeKrivulje[6] = {5, 5, 15};
	tockeKrivulje[7] = {5, 0, 17.5};
	tockeKrivulje[8] = {0, 0, 20};
	tockeKrivulje[9] = {0, 5, 22.5};
	tockeKrivulje[10] = {5, 5, 25};
	tockeKrivulje[11] = {5, 0, 22.5};

	if(argc != 2){
		printf("Nedostaje datoteka (.obj) kao argument!\n");
		exit(-1);
	}

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Racunalna grafika - lab 1");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutIdleFunc(idle);

	glutKeyboardFunc(myKeyboard);

	printf("Ucitavam datoteku %s...\n", argv[1]);

	int broj_vrhova = 0;
	int broj_poligona = 0;

	FILE *file1 = fopen(argv[1], "r");
	if(file1 != NULL){
		char line[123];
		while(fgets(line, sizeof(line), file1) != NULL){
			char type = line[0];
			if(type == 'v'){
				broj_vrhova++;
			} else if(type == 'f'){
				broj_poligona++;
			}
		}
		fclose(file1);
	} else {
		printf("Datoteka %s ne postoji ili se ne moze procitati.\n", argv[1]);
		exit(-1);
	}

	vrhovi = (struct dot*)malloc(broj_vrhova * (4 * 8) * 2);
	poligoni = (struct poli*)malloc(broj_poligona * (4 * 8 + 3 * 4) * 2);

	FILE *file2 = fopen(argv[1], "r");
	if(file2 != NULL){
		char line[123];

		while(fgets(line, sizeof(line), file2) != NULL){
			char type = line[0];
			if(type == 'v'){
				char *p;
				p = strtok(line, " ");
				double value;

				value = strtod(p, NULL);
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				vrhovi[vc].x = value;
				if(value > xmax){
					xmax = value;
				}
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				vrhovi[vc].y = value;
				if(value > ymax){
					ymax = value;
				}
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				vrhovi[vc].z = value;
				p = strtok(NULL, " ");

				vrhovi[vc].h = 1.00;
				
				vc++;
				
			} else if(type == 'f'){
				char *p;
				p = strtok(line, " ");
				double value;

				value = strtod(p, NULL);
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				poligoni[pc].p1 = value;
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				poligoni[pc].p2 = value;
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				poligoni[pc].p3 = value;
				p = strtok(NULL, " ");

				int p1 = poligoni[pc].p1 - 1;
				int p2 = poligoni[pc].p2 - 1;
				int p3 = poligoni[pc].p3 - 1;
	
				double A = ( (vrhovi[p2].y - vrhovi[p1].y) * (vrhovi[p3].z - vrhovi[p1].z) ) - 
						( (vrhovi[p2].z - vrhovi[p1].z) * (vrhovi[p3].y - vrhovi[p1].y) );

				double B = ( (vrhovi[p2].z - vrhovi[p1].z) * (vrhovi[p3].x - vrhovi[p1].x) ) -
						( (vrhovi[p2].x - vrhovi[p1].x) * (vrhovi[p3].z - vrhovi[p1].z) );

				double C = ( (vrhovi[p2].x - vrhovi[p1].x) * (vrhovi[p3].y - vrhovi[p1].y) ) - 
						( (vrhovi[p2].y - vrhovi[p1].y) * (vrhovi[p3].x - vrhovi[p1].x) );
						
				double D = ((-1) * (vrhovi[p1].x) * A) - (vrhovi[p1].y * B) - (vrhovi[p1].z * C);

				poligoni[pc].A = A;
				poligoni[pc].B = B;
				poligoni[pc].C = C;
				poligoni[pc].D = D;

				pc++;
			} else if(type == 'o'){
				char *p;
				p = strtok(line, " ");
				double value;

				value = strtod(p, NULL);
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				ociste.x = value;
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				ociste.y = value;
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				ociste.z = value;
				p = strtok(NULL, " ");

			} else if(type == 'g'){
				char *p;
				p = strtok(line, " ");
				double value;

				value = strtod(p, NULL);
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				glediste.x = value;
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				glediste.y = value;
				p = strtok(NULL, " ");

				value = strtod(p, NULL);
				glediste.z = value;
				p = strtok(NULL, " ");

			}
		}
		fclose(file2);
	} else {
		printf("Datoteka %s ne postoji ili se ne moze procitati.\n", argv[1]);
		exit(-1);
	}

	// Pohrani tocke putanje

	int putanja_c = 0;
	int tangente_c = 0;
	int tangente2_c = 0;

	for(int p = 0; p < 9; p++) {
		for(double t = 0; t <= 1; t += 0.01){
			double tx = calculatePosition(tockeKrivulje[p].x, tockeKrivulje[p + 1].x, tockeKrivulje[p + 2].x, tockeKrivulje[p + 3].x, t);
			double ty = calculatePosition(tockeKrivulje[p].y, tockeKrivulje[p + 1].y, tockeKrivulje[p + 2].y, tockeKrivulje[p + 3].y, t);
			double tz = calculatePosition(tockeKrivulje[p].z, tockeKrivulje[p + 1].z, tockeKrivulje[p + 2].z, tockeKrivulje[p + 3].z, t);

			struct dot temp;
			temp.x = tx;
			temp.y = ty;
			temp.z = tz;

			tockePutanje[putanja_c] = temp;
			putanja_c++;

			double dx = calculateDerivation(tockeKrivulje[p].x, tockeKrivulje[p + 1].x, tockeKrivulje[p + 2].x, tockeKrivulje[p + 3].x, t);
			double dy = calculateDerivation(tockeKrivulje[p].y, tockeKrivulje[p + 1].y, tockeKrivulje[p + 2].y, tockeKrivulje[p + 3].y, t);
			double dz = calculateDerivation(tockeKrivulje[p].z, tockeKrivulje[p + 1].z, tockeKrivulje[p + 2].z, tockeKrivulje[p + 3].z, t);

			temp.x = dx;
			temp.y = dy;
			temp.z = dz;

			tangentePutanje[tangente_c] = temp;
			tangente_c++;

			dx = calculate2Derivation(tockeKrivulje[p].x, tockeKrivulje[p + 1].x, tockeKrivulje[p + 2].x, tockeKrivulje[p + 3].x, t);
			dy = calculate2Derivation(tockeKrivulje[p].y, tockeKrivulje[p + 1].y, tockeKrivulje[p + 2].y, tockeKrivulje[p + 3].y, t);
			dz = calculate2Derivation(tockeKrivulje[p].z, tockeKrivulje[p + 1].z, tockeKrivulje[p + 2].z, tockeKrivulje[p + 3].z, t);

			temp.x = dx;
			temp.y = dy;
			temp.z = dz;

			tangente2Putanje[tangente2_c] = temp;
			tangente2_c++;
			}
	}

	printf("Ucitano %d vrhova i %d poligona.\n", vc, pc);
	printf("xmax: %lf ymax: %lf", xmax, ymax);
	printf("\n");

 	glutMainLoop();

	return 0;
}

void idle() {
	myDisplay();
	usleep(10000);
}

void myDisplay(){

	glColor3f(1.0f, 0.0f, 0.0f);

	for(int i = 0; i < 900; i++) {

        struct dot tocka = tockePutanje[i];

        if (crtajTangente == 1 && i % 10 == 0) {
            struct dot tangenta = tangentePutanje[i];

            glColor3f(0.4f, 0.0f, 0.0f);
            glBegin(GL_LINES);
            {
                glVertex3f(tocka.x, tocka.y, tocka.z);
                glVertex3f(tocka.x + (i % 100)*tangenta.x/100.0, tocka.y + (i % 100)*tangenta.y/100.0, tocka.z + (i % 100)*tangenta.z/100.0);
            }
            glEnd();
            glColor3f(1.0f, 0.0f, 0.0f);
        }

		glBegin(GL_POINTS);
		glVertex3f(tocka.x, tocka.y, tocka.z);
		glEnd();

	}

	glPushMatrix();

	struct dot curr_dot = tockePutanje[counter];
	struct dot curr_tan = tangentePutanje[counter];
	struct dot curr_tan2 = tangente2Putanje[counter];

	struct Vektor w = {curr_tan.x, curr_tan.y, curr_tan.z}; // tangenta
	struct Vektor w2 = {curr_tan2.x, curr_tan2.y, curr_tan2.z}; // druga derivacija

	w = normirajVektor(w);

	struct Vektor u = vektorskiUmnozak(w, w2); // normala
	u = normirajVektor(u);
	
	struct Vektor v = vektorskiUmnozak(w, u); // binormala
	v = normirajVektor(v);

	struct Matrica3x3 DCM = {w.i, u.i, v.i, w.j, u.j, v.j, w.k, u.k, v.k};
	struct Matrica3x3 DCMinv = invertirajMatricu(DCM);

	glTranslatef(curr_dot.x, curr_dot.y, curr_dot.z);

	for(int i = 0; i < pc; i++){
		
		int p1 = poligoni[i].p1 - 1;
		int p2 = poligoni[i].p2 - 1;
		int p3 = poligoni[i].p3 - 1;

		struct dot dot1 = vrhovi[p1];
		struct dot dot2 = vrhovi[p2];
		struct dot dot3 = vrhovi[p3];

        glFrontFace(GL_CCW);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        glBegin(GL_POLYGON);
			glVertex3f(dot1.x * DCMinv.ax + dot1.y * DCMinv.ay + dot1.z * DCMinv.az,
			dot1.x * DCMinv.bx + dot1.y * DCMinv.by + dot1.z * DCMinv.bz,
			dot1.x * DCMinv.cx + dot1.y * DCMinv.cy + dot1.z * DCMinv.cz);
			glVertex3f(dot2.x * DCMinv.ax + dot2.y * DCMinv.ay + dot2.z * DCMinv.az,
			dot2.x * DCMinv.bx + dot2.y * DCMinv.by + dot2.z * DCMinv.bz,
			dot2.x * DCMinv.cx + dot2.y * DCMinv.cy + dot2.z * DCMinv.cz);
			glVertex3f(dot3.x * DCMinv.ax + dot3.y * DCMinv.ay + dot3.z * DCMinv.az,
			dot3.x * DCMinv.bx + dot3.y * DCMinv.by + dot3.z * DCMinv.bz,
			dot3.x * DCMinv.cx + dot3.y * DCMinv.cy + dot3.z * DCMinv.cz);
		glEnd();

		glColor3f(1.0f, 1.0f, 1.0f);
		glPointSize(0.0);
	}

	glPopMatrix();
	glFlush();

	counter++;
	if (counter > 900){
		counter = 0;
	}

	glClear(GL_COLOR_BUFFER_BIT);
}

void myReshape(int w, int h){

	width = w; height = h; // promjena sirine i visine prozora
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);  // matrica projekcije
	glLoadIdentity();		// jedinicna matrica
    gluPerspective(60.0, (float)width/height, 0.5, 100.0);
	glMatrixMode(GL_MODELVIEW);  //	matrica pogleda
	glLoadIdentity();    // jedinicna matrica
    gluLookAt(ociste.x, ociste.y, ociste.z, glediste.x, glediste.y, glediste.z, 0.0, 0.0, 1.0);
    printf("Ociste: (%f, %f, %f) Glediste: (%f, %f, %f)\n", ociste.x, ociste.y, ociste.z, glediste.x, glediste.y, glediste.z);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // boja pozadine
	glClear(GL_COLOR_BUFFER_BIT);	// brisanje pozadine
	glPointSize(1.0);    //	postavi velicinu tocke za liniju
	glColor3f(0.0f, 0.0f, 0.0f);  // postavi boju linije

}


void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	printf("Pritisnuta tipka %c\n", theKey);
	switch (theKey)
	{
	case 'q':
		ociste.x += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'a':
		ociste.x -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'w':
		ociste.y += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 's':
		ociste.y -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'e':
		ociste.z += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'd':
		ociste.z -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;

	case 'r':
		glediste.x += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'f':
		glediste.x -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 't':
		glediste.y += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'g':
		glediste.y -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'z':
		glediste.z += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'h':
		glediste.z -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
    case 'l':
		if (crtajTangente == 1){
            crtajTangente = 0;
        } else {
            crtajTangente = 1;
        }
		myReshape(width, height);
		myDisplay();
		break;
	}
	
}

