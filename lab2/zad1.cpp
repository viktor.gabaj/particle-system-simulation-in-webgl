#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <GL/glut.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

GLuint window;
GLuint width = 1000, height = 1000;
int counter = 0;

void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void idle();

struct Flake {
	double lifetime;
	double age = 0.0;
	double x;
	double y;
	double weight;
	double survived;
} flake;

struct dot {
	double x;
	double y;
	double z;
	double h;
} dot;

static int N = 100;
Flake flakes[100][100];

double xmax = 0, ymax = 0;
struct dot ociste, glediste;

struct dot *vrhovi;
struct poli *poligoni;
int vc = 0, pc = 0;

double density = 0.99;

double randIn(double a, double b) {
	return a + ((rand()%100)/100.0)*(b - a);
}


Flake makeFlake() {
	Flake tmp;
	tmp.lifetime = randIn(0.4, 1.0);
	tmp.x = randIn(0, 10);
	tmp.y = randIn(0, 10);
	tmp.weight = randIn(10, 20);
	if((rand()%100/100.0) < density) {
		tmp.survived = 0.0;
	} else {
		tmp.survived = 1.0;
	}
	return tmp;
}

int main(int argc, char ** argv){

	ociste.x = 15;
	ociste.y = 15;
	ociste.z = 8;
	glediste.x = 0;
	glediste.y = 0;
	glediste.z = 0;

	// initiate flakes

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N; j++){
			flakes[i][j] = makeFlake();
			flakes[i][j].age = (rand()%100/100.0)*flakes[i][j].lifetime;
		}
	}

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Racunalna grafika - lab 1");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutIdleFunc(idle);

	glutKeyboardFunc(myKeyboard);

 	glutMainLoop();

	return 0;
}

void idle() {
	myDisplay();
	usleep(30500);
}

void myDisplay(){

	// draw coordinate sys

	glColor3f(1.0f, 0, 0);
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(100, 0, 0);
	glEnd();

	glColor3f(0, 1.0f, 0);
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 100, 0);
	glEnd();

	glColor3f(0, 0, 1.0f);
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, 100);
	glEnd();


	// draw ground
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_POLYGON);
	{
		glVertex3f(0, 0, 0);
        glVertex3f(10, 0, 0);
		glVertex3f(0, 10, 0);
	}
	glEnd();
	glBegin(GL_POLYGON);
	{
		glVertex3f(10, 10, 0);
        glVertex3f(10, 0, 0);
		glVertex3f(0, 10, 0);
	}
	glEnd();

	// draw flakes

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N; j++) {
			flakes[i][j].age += 0.01;

			if(flakes[i][j].age > flakes[i][j].lifetime){
				flakes[i][j] = makeFlake();
			}

			double opacity = 1 - (flakes[i][j].age / flakes[i][j].lifetime) + 0.2;
			glPointSize(3.0*(1 - opacity));
			glBegin(GL_POINTS);
			glColor3f(opacity * flakes[i][j].survived, opacity * flakes[i][j].survived, opacity * flakes[i][j].survived);
			glVertex3f(flakes[i][j].x, flakes[i][j].y, (10 - flakes[i][j].age * flakes[i][j].age * flakes[i][j].weight));
			glEnd();
			
		}
	}
	
	glFlush();
	glClear(GL_COLOR_BUFFER_BIT);

}

void myReshape(int w, int h){

	width = w; height = h; // promjena sirine i visine prozora
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);  // matrica projekcije
	glLoadIdentity();		// jedinicna matrica
    gluPerspective(60.0, (float)width/height, 0.5, 100.0);
	glMatrixMode(GL_MODELVIEW);  //	matrica pogleda
	glLoadIdentity();    // jedinicna matrica
    gluLookAt(ociste.x, ociste.y, ociste.z, glediste.x, glediste.y, glediste.z, 0.0, 0.0, 1.0);
    printf("Ociste: (%f, %f, %f) Glediste: (%f, %f, %f)\n", ociste.x, ociste.y, ociste.z, glediste.x, glediste.y, glediste.z);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // boja pozadine
	glClear(GL_COLOR_BUFFER_BIT);	// brisanje pozadine 
	glColor3f(0.0f, 0.0f, 0.0f);  // postavi boju linije

}


void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	printf("Pritisnuta tipka %c\n", theKey);
	switch (theKey)
	{
	case 'q':
		ociste.x += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'a':
		ociste.x -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'w':
		ociste.y += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 's':
		ociste.y -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'e':
		ociste.z += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'd':
		ociste.z -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;

	case 'r':
		glediste.x += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'f':
		glediste.x -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 't':
		glediste.y += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'g':
		glediste.y -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'z':
		glediste.z += 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case 'h':
		glediste.z -= 0.5;
		myReshape(width, height);
		myDisplay();
		break;
	case '1':
		density = 0.99;
		break;
	case '2':
		density = 0.6;
		break;
	case '3':
		density = 0.0;
		break;
	}
}

