import './App.css';
import React from 'react';
import * as THREE from 'three';
import autoBind from 'auto-bind';

var screenW = window.innerWidth;
var screenH = window.innerHeight; 

var spdx = 0; 
var spdy = 0; 
var mouseX = 0; 
var mouseY = 0; 
var startX = 0;
var startY = 0;
var mouseDown = false; 

document.addEventListener('mousemove', function(event) {
  if(event.target.className === "settings" || event.target.className === "slider") {
    return;
  }
  mouseX = startX - event.clientX;
  mouseY = startY - event.clientY;
}, false);
document.body.addEventListener("mousedown", function(event) {
  if(event.target.className === "settings" || event.target.className === "slider") {
    return;
  }
  mouseDown = true
  startX = event.clientX;
  startY = event.clientY;
}, false);
document.body.addEventListener("mouseup", function(event) {
  if(event.target.className === "settings" || event.target.className === "slider") {
    return;
  }
  mouseDown = false
}, false);

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
var renderer = new THREE.WebGLRenderer();

class App extends React.Component {

  state = {
    cameraX: 6,
    cameraY: -6,
    cameraZ: 18,
    density: 10,
    windValue: 0,
    gravity: 1,
    temperature: -10
  }

  constructor(props) {
    super(props);
    autoBind(this);
  }

  changeCameraX(event) {
    this.setState({...this.state, cameraX: event.target.value});
  }
  changeCameraY(event) {
    this.setState({...this.state, cameraY: event.target.value});
  }
  changeCameraZ(event) {
    this.setState({...this.state, cameraZ: event.target.value});
  }
  changeN(event) {
    this.setState({...this.state, N: event.target.value});
  }
  changeDensity(event){
    this.setState({...this.state, density: event.target.value});
  } 
  changeWind(event) {
    this.setState({...this.state, windValue: event.target.value});
  }
  changeGravity(event) {
    this.setState({...this.state, gravity: event.target.value});
  }
  changeTemp(event) {
    this.setState({...this.state, temperature: event.target.value});
  }

  makeParticle() {
      var geometry, material;
      if(this.state.temperature <= 0) {
        geometry = new THREE.SphereGeometry( 0.06, 3, 3);
        material = new THREE.MeshBasicMaterial( { color: 0xffffff } );
      } else {
        geometry = new THREE.BoxGeometry( 0.02, 0.02, 0.1);
        material = new THREE.MeshBasicMaterial( { color: 0x46DDF1 } );
      }
      material.transparent = true;
      const particle = new THREE.Mesh(geometry, material);
      particle.position.x = Math.random() * 10.0;
      particle.position.y = Math.random() * 10.0;
      particle.position.z = Math.random() * 10.0;
      particle.userData.weight = Math.random();
      particle.userData.alive = 1;
      return particle;
  };

  componentDidMount() {
    this.renderScene();
  }

  renderScene() {

      renderer.renderLists.dispose();

      scene.remove.apply(scene, scene.children);
      renderer.setSize( window.innerWidth, window.innerHeight );
      const container = document.getElementById('container');
      if(container.firstChild != null) {
        container.removeChild(container.firstChild);
      }
      container.appendChild(renderer.domElement);

      const particles = [];

      for(var i = 0; i < 6000; i++) {
        const particle = this.makeParticle();
        particles.push(particle);
      }

      for(i = 0; i < particles.length; i++) {
        scene.add(particles[i]);
      }

      // polygon cloud
      const polyMaterial = new THREE.LineBasicMaterial( { color: 0x0000ff } );
      const points = [];
      points.push( new THREE.Vector3( 0, 0, 10 ) );
      points.push( new THREE.Vector3( 0, 10, 10 ) );
      points.push( new THREE.Vector3( 10, 10, 10 ) );
      points.push( new THREE.Vector3( 10, 0, 10 ) );
      points.push( new THREE.Vector3( 0, 0, 10 ) );

      const polyGeometry = new THREE.BufferGeometry().setFromPoints( points );
      const line = new THREE.Line( polyGeometry, polyMaterial );
      scene.add( line );

      // coordinates
      const xmat = new THREE.LineBasicMaterial( { color: 0xff0000 } );
      const xpoints = [];
      xpoints.push( new THREE.Vector3( 0, 0, 0 ) );
      xpoints.push( new THREE.Vector3( 10000, 0, 0 ) );
      const xgeo = new THREE.BufferGeometry().setFromPoints( xpoints );
      const xline = new THREE.Line( xgeo, xmat);
      scene.add( xline );

      const ymat = new THREE.LineBasicMaterial( { color: 0x00ff00 } );
      const ypoints = [];
      ypoints.push( new THREE.Vector3( 0, 0, 0 ) );
      ypoints.push( new THREE.Vector3( 0, 10000, 0 ) );
      const ygeo = new THREE.BufferGeometry().setFromPoints( ypoints );
      const yline = new THREE.Line( ygeo, ymat);
      scene.add( yline );

      const zmat = new THREE.LineBasicMaterial( { color: 0x0000ff } );
      const zpoints = [];
      zpoints.push( new THREE.Vector3( 0, 0, 0 ) );
      zpoints.push( new THREE.Vector3( 0, 0, 10000 ) );
      const zgeo = new THREE.BufferGeometry().setFromPoints( zpoints );
      const zline = new THREE.Line( zgeo, zmat);
      scene.add( zline );

      // Camera position
      camera.position.x = this.state.cameraX;
      camera.position.y = camera.position.y || this.state.cameraY;
      camera.position.z = camera.position.z || this.state.cameraZ;
      camera.up = new THREE.Vector3(0, 0, 1);

      camera.lookAt(5, 5, 5);

      var animate = () => {
        requestAnimationFrame( animate );

        for(var i = 0; i < particles.length; i++) {
            const particleAge = particles[i].position.z / 10.0;

            if(particleAge < 0.2) {
              particles[i].position.x = Math.random() * 10.0;
              particles[i].position.y = Math.random() * 10.0;
              particles[i].position.z = 10;
              if(Math.random() <= this.state.density/100){
                particles[i].userData.alive = 1;
              } else {
                particles[i].userData.alive = 0;
              }
            }

            particles[i].position.x += this.state.windValue/100;
            particles[i].position.z -= 0.02 + particles[i].userData.weight * this.state.gravity/10;
            particles[i].material.opacity = particleAge * 1.2 * particles[i].userData.alive;
            particles[i].scale.x = particleAge;
            particles[i].scale.y = particleAge;
            particles[i].scale.z = particleAge;
          
            spdx = mouseX/screenW;
            spdy = mouseY/screenH;
            if(mouseDown) {
              camera.position.y += spdx/100;
              camera.position.z -= spdy/100;
              camera.lookAt(5, 5, 5);
            }
        }
        
        renderer.render( scene, camera );
            
      };
      animate();
  }

  render() {


    return (
      <React.Fragment>
        <div className="settings">

          <div className="slidecontainer">
            <p>Density: {this.state.density}</p>
            <input onChange={this.changeDensity} type="range" min="0" max="10" value={this.state.density} className="slider" id="myRange" />
          </div>

          <div className="slidecontainer">
          <p>Gravity: {this.state.gravity}</p>
            <input onChange={this.changeGravity} type="range" min="0" max="10" value={this.state.gravity} className="slider" id="myRange" />
          </div>

          <div className="slidecontainer">
            <p>Wind: {this.state.windValue}</p>
            <input onChange={this.changeWind} type="range" min="-30" max="30" value={this.state.windValue} className="slider" id="myRange" />
          </div>

          <div className="slidecontainer">
            <p>
            <span>Temperature: {this.state.temperature} °C <button onClick={() => this.renderScene()}>Apply</button></span></p>
            <input onChange={this.changeTemp} type="range" min="-10" max="30" value={this.state.temperature} className="slider" id="myRange" />
          </div>

        </div>
        <div id='container' style={{'backgroundColor':'black', 'width': '100vw', 'height':'100vh'}} className='container' />
    </React.Fragment>      
    );
  };
  
}

export default App;
